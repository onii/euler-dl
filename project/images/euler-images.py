#!/usr/bin/python3
from bs4 import BeautifulSoup
import shutil
import requests
import time

soup = BeautifulSoup(open("../../euler.html"), "lxml")
images = soup.find_all('img')
for index, item in enumerate(images):
    url = ('https://projecteuler.net/' + item.get('src'))
    filename = url[url.rfind("/")+1:]
    print(index+1,"/",len(images))
    for yep in range(25):
        while True:
            try:
                response = requests.get(url, stream=True)
                break
            except Exception:
                time.sleep(10)
                print("Script having connection issues. (" + time.strftime("%c") + ")")
                continue
        break
    with open(filename, 'wb') as filename:
        shutil.copyfileobj(response.raw, filename)
    del response
    time.sleep(1)
