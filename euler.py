#!/usr/bin/python3
import requests
import re
import time
from bs4 import BeautifulSoup

archive = requests.get('https://projecteuler.net/archives')
soup = BeautifulSoup(archive.content, "lxml")
archiveList = soup.find('div',{'class':'pagination'})
problemsList = soup.find('p')
problemNum = re.sub("\D","",problemsList.contents[0])[1:][:-2]

for i in range(int(problemNum)):
    for yep in range(100):
        while True:
            try:
                probPage = requests.get('https://projecteuler.net/problem='+ str(i+1))
                break
            except Exception:
                time.sleep(20)
                print("Script having connection issues. (" + time.strftime("%c") + ")")
                continue
        break
    soup = BeautifulSoup(probPage.content, "lxml")
    problem = soup.find('div',{'class':'problem_content'})
    with open("euler.html", "a") as myfile:
        print("Page: " + str(i+1) + "/" + problemNum)
        myfile.write("<h2>Problem: " + str(i+1) + '</h2>')
        for item in problem:
            myfile.write("%s\n" % item)
    time.sleep(1)
