# This script downloads every problem from [Project Euler](https://projecteuler.net) for offline use.

Requires `python3-bs4` and `python3-requests`.

If you want to make a nice file out of it for ereaders, etc:

    pandoc -s -o eulers-problems_5-7-16.epub -f html euler.html